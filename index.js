// console.log("Hello World");

// [SECTION] Functions
// Functions in JS are lines/block of codes that tell our device/application to perform a certain task when called/invided.
// They are also used to prevent repeating lines/block of codes that perfrom the same task/function.

// Function Declaration 
	// Function statement defines a function with parameters 

	// function declaration is also called function keyword - use to define a javascript function
	// function name - is used so we are able to call/invoke a declared function ({}) - the statement which compromise the body of the function. This is where the code to be executed.

// function declaration // function name 
// funtion name required open and close parenthesis beside it
function printName () { // code block
	console.log("My name is John"); // function statement
}; // delimeter

printName(); // function invocation



// [HOISTING]
// hoisting means you can call a function before the declaration

declaredFunction();
// make sure the function is existing whenever we call/invoice a function 

function declaredFunction(){
console.log("Hello World");
};


// [Function Expression]
// A function can be also stored in a variable. Thas is call as function expression
// Hoisting is allowed in function declaration but not in function expresion
let variableFunction = function (){
	console.log("Hello again");
};

variableFunction();


let funcExpression = function funcName(){
	console.log("Hello from the other side.");
};

funcExpression();
// funcName();
// Error: we just call the variable name, not the function name

console.log("---------------------");
console.log("[Reasssigning Function]");

declaredFunction();
declaredFunction = function() {
	console.log("updated declaredFunction");
}

declaredFunction();


funcExpression = function (){
	console.log("updated funcExpression");

}

funcExpression();




//  Constatant Function

const constantFuction = function() {
	console.log("Initialized with const.");
}

constantFuction();

/* 
	constantFuction = function(){
	console.log("New value");

}

constantFuction();

*/

// function with const keyword cannot be reassigned.
// scope is the accessibility/visibility of a variables in the code

/* 
	JS variables has 3 scopes
	1. local scope
	2. global scope
	3. function scope

*/


console.log("---------------------");
console.log("[Function Scoping]");


{

	let localVar = "Armando Perez";
	console.log(localVar);
}

 // console.log(localVar);


let globalVar = "Mr. Worldwide";
console.log(globalVar);


{
console.log(globalVar);
}


// [Function Scoping]
// Variables define inside a function are not accessible/visible outside the function 
// Variable declared with var, let and const are quite similar when declared inside a function
function showNames(){
	var functionVar = "Joe";
	const functionConst = "John";
	let functionLet = "Jane";

	console.log(functionVar);
	console.log(functionConst);
	console.log(functionLet);
}

showNames();


// Error - This are function scoped variable anf cannot be acced outside the function they were declared in 
//	console.log(functionVar);
//	console.log(functionConst);
//	console.log(functionLet);


// Nested Functions 
	// You can create another function inside a function, called Nested Function 


function myNewFunction(){
	let name = "Jane";

	function nestedFunction(){
		let nestedName = "John";
		console.log(name);

	}
	nestedFunction();
	console.log(nestedFunction);
	}

	myNewFunction();



// Global Scoped Variable
let globalName = "Zuitt";

function myNewFunction2(){
	let nameInside = "Renz";
	console.log(globalName);
}

myNewFunction2();


// alert () allows us to show a small window at the top of our browser page to show information to our users.

// alert("Sample Alert");

function sampleAlert(){
	alert("Hello, user!");
}

sampleAlert();

// alert messages inside a function will only execute whenever we call/invoice the function 

console.log("I will only log in the consol when the alert is dismissed");





// Notes on the use of alert();
	// Show only an alert for short dialogs/messages to the user
	// Do not overuse alert() because the program/js has to wait fot it to be dismissed before continuing 





// [Prompt]
	// prompt() allow us to show small window at the top of the browser to gather user input

/*
let samplePrompt = prompt("Enter your Full Name")
//console.log(samplePrompt);

console.log(typeof samplePrompt);


// you can concatenate message in prompt
console.log("Hello" + samplePrompt);
*/





function printWelcomeMessage(){
	let firstName = prompt("Enter your first name: ");
	let lastName = prompt ("Enter your last name: ");


	console.log("Hello " + firstName + " " + lastName + "!");
	console.log("Welcome to my page!");
}

printWelcomeMessage();




// [SECTION] Function Naming Convention 

// Function name should be definitive of the task it will perform. It usually contains a verb

function getCourses(){
	let courses = ["Science 101", "Math 101", "English 101"];
	console.log(courses);
}

getCourses();


// avoid generic name to avoid confusion within our code.

function get (){
	let name = "Jamie";
	console.log(name);
}

// Avoid pointless and inappropriate function names, example: foo, bar, etc. 

function foo (){
	console.log(25%5);
}

foo();


// Name your function in small caps. Follow camelCase when namin variables and functions 

function displayCarInfor(){
	console.log("Brand: Toyota");
	console.log("Type: Sedan");
	console.log("Price: 1,500,000");

}

displayCarInfor();


















